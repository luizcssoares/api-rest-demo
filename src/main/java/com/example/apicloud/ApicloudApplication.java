package com.example.apicloud;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ApicloudApplication {

	public static void main(String[] args) {
		SpringApplication.run(ApicloudApplication.class, args);
	}

}
